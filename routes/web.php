<?php

use App\Http\Controllers\UserController;

Route::redirect('/', '/posts');
Route::redirect('/home', '/posts');

Route::get('/posts', 'PostController@index');
Route::get('/posts/create', 'PostController@create');
Route::post('/posts', 'PostController@store');
Route::get('/posts/myPosts', 'PostController@userPosts');
Route::get('/posts/{id}', 'PostController@show')->name('post');
Route::post('/comments', 'CommentController@store');
Route::get('/admin/users', 'UserController@index');
Route::get('/admin/users/{id}/destroy', 'UserController@destroy');
Route::get('/admin/users/{users}', 'UserController@destroy');
Route::get('/admin/users/{users}', 'UserController@update');
Route::get('/admin/users/{users}/edit', 'UserController@edit');


Route::get('/notifi', function () {
    return view('posts.notifi');

})->name('notifi');



Auth::routes();

//Route::view('/posts/create', 'create');
//Route::post('/posts/create', 'PostController@create');
//Route::get('/posts/{id}', 'PostController@Show');
//Route::get('/today', 'PostController@today');


//Route::get('/home', 'PostController@index')->name('home');
//Route::get('/home', 'HomeController@index')->name('home');
