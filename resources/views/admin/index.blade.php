@extends('layouts.app')

@section('content')
    <div class="col-sm-7">
        <table class="table table-striped text-sm-center">
            <thead>
                <th>Nombre</th>
                <th>Email</th>
                <th>Acción</th>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td><a href="{{ action('UserController@edit', $user->id) }}" class="btn btn-warning">Editar</a>

                            <a href="{{ action('UserController@destroy', $user->id) }}" onclick="return confirm(
                                '¿Estas seguro de eliminarlo?')" class="btn btn-danger">Eliminar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
