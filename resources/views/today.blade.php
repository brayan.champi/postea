@extends('layouts.app')

@section('content')
    <div class="container text-center card-title">
        <h1>Publicaciones realizadas durante las 24hrs.</h1>
    </div>
    <div class="container">
        @foreach($publicaciones as $publicacion)
            <div class="row mb-4 justify-content-md-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                <!--Llamamos a PostController y mediante la publicacion mostramos el titulo -->
                                <a href="{{ action('PostController@Show', $publicacion->id) }}">{{ $publicacion->title }}</a>
                            </h5>
                        </div>
                        <img src="{{asset($publicacion->image) }}" class="card-img-top" alt="...">
                        <!-- Mediante la publicacion mostramos la imagen -->
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
