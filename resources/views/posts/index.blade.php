@extends('layouts.app')

@section('content')
<div class="form-group text-center">
    <div class="button">
        <a class="btn btn-primary col-sm-4"  href="{{ action('PostController@userPosts') }}" role="button">Mis Posts</a>
    </div>
    <hr>
</div>
<div class="container">
    @foreach($publicaciones as $publicacion)
    <div class="row mb-4 justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">
                        <!--Llamamos a PostController y mediante la publicacion mostramos el titulo -->
                        <a href="{{ action('PostController@show', $publicacion->id) }}">{{ $publicacion->title }}</a>
                    </h5>
                </div>
                <img src="{{ asset($publicacion->image) }}" class="card-img-top" alt="...">
                <!-- Mediante la publicacion mostramos la imagen -->
            </div>
        </div>
    </div>
    @endforeach
    {{ $publicaciones->links() }}
</div>
@endsection

