@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h2>TUS NOTIFICACIONES</h2>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped text-sm-center col-sm-10">
                <thead>
                <th>Respuesta</th>
                <th>Fecha Creación</th>
                </thead>
                <tbody>
                @foreach(Auth::user()->notifications as $notifi)
                    <tr>
                        <td>{{ $notifi->data['content'] }}</td>
                        <td>{{ $notifi->created_at }}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
