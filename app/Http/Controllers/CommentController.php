<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Notifications\InvoicePaid;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required:max:250',
        ]);

        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->content = $request->get('content');

        $post = Post::find($request->get('post_id'));
        $propietario_id = $post->user_id;
        $propietario = User::where('_id', '=', $propietario_id)->first();
        $post->comments()->save($comment);

        $propietario->notify(new InvoicePaid($post->id, $comment->user_id));

        return redirect()->route('post', ['id' => $request->get('post_id')]);
    }
}
