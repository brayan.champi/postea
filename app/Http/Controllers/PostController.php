<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use App\Post;
use MongoDB\Driver\Session;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $publicaciones = Post::orderBy('id', 'ASC')->paginate(10);
        return view('posts.index', compact('publicaciones'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function show($id)
    {
        return view('posts.postUnico', ['post' => Post::find($id)]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required:max:120',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required:max:2200',
        ]);

        //$image = $request->file('image');
        //$imageName = time() . $image->getClientOriginalName();
        $imageName = $request->file('image')->store('posts/' . Auth::id(), 'public');
        $title = $request->get('title');
        $content = $request->get('content');

        $post = $request->user()->posts()->create([
            'title' => $title,
            //'image' => 'img/' . $imageName,
            'image' => $imageName,
            'content' => $content,
        ]);

        //$request->image->move(public_path('img'), $imageName);

        return redirect()->route('post', ['id' => $post->id]);
    }



    # Función para mostrar los posts del usuario logeado
    public function userPosts()
    {
        # Buscamos al usuario por su identificador único
        $user_id = Auth::id();
        # Nos retorma las publicaciones de nuestra vista index
        $publicaciones = Post::where('user_id', '=', $user_id)->orderBy('id', 'ASC')->paginate(5);
        return view('posts.index', compact('publicaciones'));
    }


    public function destroy($id)
    {
        # Borramos nuestra publicación
        $post = Post::find($id);
        $post->delete();

        # Retornamos nuevamente a nuestras publicaciones
        return redirect()->route('posts.postUnico');
    }
}
